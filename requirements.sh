#!/usr/bin/env bash
# 
# Instala los necesario para compilar
# el documento LaTeX con el Makefile.
#

# Compilador LaTeX y paquetes.
sudo apt-get install --assume-yes texlive-full

# Gestor de bibliografía en LaTeX.
sudo apt-get install --assume-yes biber

# Procesador de texto LaTeX.
sudo apt-get install --assume-yes textstudio

# Herramienta utilizada por el paquete 'minted'.
sudo apt-get install --assume-yes python-pygments python-pygments-doc
sudo pip install Pygments