#!/usr/bin/env bash
# Generate *.eps from *.dot

_dots=$(find . -type f -name "*.dot")
for _dot in ${_dots}
do
  _basename=$(basename ${_dot} | cut -d. -f1)
  dot -Tps ${_dot} > ../${_basename}.eps
done
