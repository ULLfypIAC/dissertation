% !TeX root = ../../document.tex
% !TeX encoding = UTF-8
% !TeX spellcheck = es
% ---------------------------------------------------------------------
% ---------------------------------------------------------------------
% ---------------------------------------------------------------------

\chapter{Cuestiones previas a la implementación}
\label{ch:03}

\section{Descripción y configuración utilizada para la ejecución de pruebas}
\label{ch:03:sec:01}

Antes de la realización de cualquier prueba describiremos algunas cuestiones importantes, como las siguientes:

\begin{itemize}
    \item Configuración hardware y software utilizada en las pruebas.
    \item Ficheros de datos (\textbf{fichero madre} y \textbf{fichero crowding de entrada}).
    \item Parámetros de entrada (\textbf{características}, \textbf{valores de error}, \textbf{N} y \textbf{tolerancia}).
    \item Formato de presentación de las pruebas.
\end{itemize}

La configuración hardware y software del equipo utilizado para realizar cualquier prueba de rendimiento es la siguiente:

\begin{itemize}

    \item Especificaciones Hardware:

    \begin{itemize}
        \item Procesador: Intel Core i7-4930K, 3.4 GHz.
        \item Memoria \acrshort{RAM}: 32 GBytes DDR3, 1333 MHz.
        \item Tarjeta Gráfica: Nvidia GeForce Titan Black, GPU GK110, 967 MHZ. 2880 CUDA cores. 6 GBytes \acrshort{VRAM} GDDR5 a 7000 MHz.
    \end{itemize}

    \item Especificaciones Software:

    \begin{itemize}
        \item Sistema Operativo \gls{Linux}. Distribución \gls{Kubuntu} v15.04
        \item \acrshort{GCC} v4.9.2
        \item \gls{OpenMP} 4.0
        \item \gls{CUDA Toolkit} 7.5
    \end{itemize}
\end{itemize}

Para realizar las pruebas se han utilizado los siguientes pares de ficheros de entrada:

\begin{itemize}

    \item Primer par de ficheros entrada:

    \begin{itemize}

        \item Fichero Madre:

        \begin{itemize}

            \item basti\_0002\_008\_cuadrado\_all\_reducidoXY\_nuevo2.databs

            \item 1500000 líneas

        \end{itemize}

        \item Fichero Crowding de entrada:

        \begin{itemize}

            \item crow\_m1\_nuevo2.databs

            \item 254418 líneas

        \end{itemize}

    \end{itemize}

    \item Segundo par de ficheros de entrada:

    \begin{itemize}

        \item Fichero Madre:

        \begin{itemize}

            \item lmc\_johnson\_reducidoXY.databs

            \item 16000000 líneas

        \end{itemize}

        \item Fichero Crowding de entrada:

        \begin{itemize}

            \item f10.comp.calabs

            \item 11907631 líneas

        \end{itemize}

    \end{itemize}

\end{itemize}

Las \textbf{características utilizadas} cuando el número de condiciones es cuatro, son las siguientes (la forma de denotar los datos es la misma que en el apartado \ref{ch:02:sec:02}, párrafo 2):

\begin{itemize}

    \item Característica 1:

    \begin{itemize}

        \item Columna 1. Fichero madre (\textbf{M1}).

        \item Columna 1. Fichero crowding de entrada (\textbf{M1in}).

    \end{itemize}

    \item Característica 2:

    \begin{itemize}

        \item Columna 1 - Columna 2. Fichero madre (\textbf{M1 - M2}).

        \item Columna 1 - Columna 2. Fichero crowding de entrada (\textbf{M1in - M2in}).

    \end{itemize}

    \item Característica 3:

    \begin{itemize}

        \item Columna 5. Fichero madre (\textbf{X}).

        \item Columna 5. Fichero crowding de entrada (\textbf{X}).

    \end{itemize}

    \item Característica 4:

    \begin{itemize}

        \item Columna 6. Fichero madre (\textbf{Y}).

        \item Columna 6. Fichero crowding de entrada (\textbf{Y}).

    \end{itemize}

\end{itemize}

NOTA: Cuando el número de condiciones es \textbf{dos}, \textbf{sólo} se usarán \textbf{las dos primeras} características anteriores.

Los \textbf{valores de intervalo} utilizados para cada una de las características son los siguientes:

\begin{itemize}

    \item Valor de error para la característica 1:

    \begin{itemize}

        \item $\textbf{$\varepsilon$}_{\textbf{m}} = 0.1 $

    \end{itemize}

    \item Valor de error para la característica 2:

    \begin{itemize}

        \item $\textbf{$\varepsilon$}_{\textbf{c}} = 0.04 $

    \end{itemize}

    \item Valor de error para la característica 3:

    \begin{itemize}

        \item $\textbf{$\varepsilon$}_{\textbf{x}} = 300.0 $

    \end{itemize}

    \item Valor de error para la característica 4:

    \begin{itemize}

        \item $\textbf{$\varepsilon$}_{\textbf{y}} = 300.0 $

    \end{itemize}

\end{itemize}

NOTA: Cuando el número de condiciones es \textbf{dos}, \textbf{sólo} se usarán \textbf{los dos primeros} valores de error anteriores.

El número mínimo de estrellas (\textbf{N}) que se debe seleccionar en las búsquedas es 10.

El valor de \textbf{tolerancia} se ha establecido en 25.0.

Los ficheros de datos y parámetros de entrada fueron proporcionados por el \acrshort{IAC}.

Las pruebas realizadas se mostrarán en forma de tabla de cuatro columnas. Cada tabla tendrá una fila de cabecera que especificará el \textbf{número de condiciones} con las que se ha trabajado, el \textbf{par de ficheros} que se ha usado y el \textbf{tipo de versión} que indica si se ha usado o no la estructura especial que acelera las búsquedas (\textbf{nm} -> no se ha usado, \textbf{avl} -> si se ha usado). 

Cada fila de una tabla constituye una prueba donde:

\begin{itemize}

    \item La primera columna indica el número de estrellas madres que intervienen en la prueba (n-primeras estrellas del fichero madre correspondiente). El nombre de esta columna será \textbf{Estrellas procesadas}.

    \item La \textbf{segunda columna} indica el tiempo consumido por la prueba usando sólo un núcleo del procesador. El nombre de esta columna será \textbf{SECUENCIAL}.

    \item La \textbf{tercera columna} indica el tiempo consumido por la prueba usando todos los núcleos del procesador (paralelismo a nivel de \acrshort{CPU}). El nombre de esta columna será \textbf{OPENMP}.

    \item La \textbf{cuarta columna} indica el tiempo consumido por la prueba usando los núcleos de la tarjeta gráfica (paralelismo a nivel de \acrshort{GPU} de la prueba). El nombre de esta columna será \textbf{CUDA}.

\end{itemize}

De esta forma una celda, muestra el tiempo de ejecución de la prueba: SECUENCIAL, OPENMP (paralelismo a nivel de \acrshort{CPU}) ó CUDA (paralelismo a nivel de \acrshort{GPU}) dependiendo de la columna que miremos. Los valores de tiempo mostrados en las celdas están en \textbf{segundos}.

Para una mejor comprensión e interpretación de las tablas, cada una ellas tendrá asociada una gráfica en la que se mostrarán los tiempos obtenidos sobre el número de estrellas madre procesadas.

Para realizar las ejecuciones de las distintas pruebas se ha utilizado el \textbf{comando timeout}. Este comando sirve para establecer el \textbf{tiempo límite} que un determinado comando está en ejecución. Se ha establecido un tiempo límite de \textbf{2000 segundos} para la ejecución de cualquier prueba. Una celda vacía en una tabla significa que la prueba ha alcanzado el tiempo límite. Una celda vacía en una tabla significa que la prueba ha alcanzado el tiempo límite.

\section{Validación de los resultados obtenidos en las pruebas}
\label{ch:03:sec:02}

Para llevar a cabo la validación de los resultados obtenidos en las pruebas se utiliza el fichero de selecciones, una versión reducida del fichero dispersado. Cada línea de este fichero representa una estrella madre a la que se le ha aplicado el proceso explicado en el apartado \ref{ch:02:sec:02} y consta de tres datos (columnas):

\begin{itemize}
    \item El \textbf{primero} es un identificador natural de estrella madre. Este identificador se corresponde con el número de línea que ocupa la estrella madre en el fichero madre.
    \item El \textbf{segundo} es la séptima columna del fichero dispersado.
    \item El \textbf{tercero} es la octava columna del fichero dispersado.
\end{itemize}

Una vez realizada la implementación del pseudocódigo del apartado \ref{ch:02:sec:02}, se usó el primer par de ficheros (ver apartado \ref{ch:03:sec:01}) y los parámetros de entrada (características, valores de error, N y tolerancia, ver apartado \ref{ch:03:sec:01}). Con esta configuración se realizaron dos ejecuciones, una ejecución en la que las búsquedas tenían dos condiciones y otra ejecución con cuatro condiciones. Los resultados obtenidos en ambas ejecuciones, los dos ficheros de selecciones, fueron enviados a los responsables del \acrshort{IAC} para su validación.

En el momento en el que los resultados obtenidos obtuvieron el visto bueno por parte de los responsables del \acrshort{IAC}, se comenzó con la realización de pruebas sistemáticas.

Para comprobar el correcto funcionamiento de futuras nuevas versiones e implementaciones, el fichero de selecciones obtenido se compara con el correspondiente fichero de selecciones validado. La nueva versión \textbf{es correcta si las dos últimas columnas de ambos ficheros no presentan diferencias}.

% ---------------------------------------------------------------------
% ---------------------------------------------------------------------
% ---------------------------------------------------------------------