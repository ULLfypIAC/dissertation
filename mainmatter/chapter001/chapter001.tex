% !TeX root = ../../document.tex
% !TeX encoding = UTF-8
% !TeX spellcheck = es
% ---------------------------------------------------------------------
% ---------------------------------------------------------------------
% ---------------------------------------------------------------------
\chapter{Introducción}
\label{ch:01}

\section{Descripción general del problema y consideraciones}
\label{ch:01:sec:01}

En la primera parte de este proyecto se pretende resolver el siguiente problema:

Consideremos dados dos conjuntos de datos: un \textbf{conjunto A} y un \textbf{conjunto B}.

Para cada elemento del conjunto A, se debe encontrar aquellos elementos de B que más se parecen al elemento seleccionado en A, una vez examinado todo el conjunto B y obtenidos todos los elementos semejantes, nos quedamos con uno de esos elementos semejantes de forma aleatoria. Ese elemento semejante obtenido de forma aleatoria, después de algunas transformaciones, constituye la salida del problema para el elemento seleccionado en A.

Por cada elemento del conjunto A, se genera un elemento de salida y es por ello que la salida de nuestro problema no es más que otro conjunto que llamamos, \textbf{conjunto C} y cuyo tamaño y formato es idéntico al del conjunto A. También se genera como salida un \textbf{conjunto D} de igual tamaño y formato que el conjunto B que describiremos en detalle más adelante.

Es importante hacer la siguiente consideración y es que el tamaño de los conjuntos A y B es bastante significativo. El cardinal del conjunto B suele ser menor que el cardinal del conjunto A, pero, aún así estamos tratando con conjuntos cuyo cardinal alcanza los \textbf{10 millones como mínimo}. Durante el desarrollo del proyecto se ha trabajado con los siguientes cardinales, en el \textbf{conjunto A de 16 millones} y en el \textbf{conjunto B de 12 millones aproximadamente}.

Como se ha indicado anteriormente el cardinal de \textbf{ambos conjuntos} es grande y el problema que nos ocupa es un \textbf{problema de exploración} (\textbf{búsqueda}) dentro de un conjunto (conjunto B), \textbf{dado un elemento de otro conjunto} (conjunto A).

Por tanto, la clave para resolver este problema en el menor tiempo posible, consiste en  disponer de alguna \textbf{organización especial} (\textbf{estructura}) para los elementos del conjunto B, que minimice el tiempo de búsqueda. Es de suma importancia destacar que \textbf{los elementos del conjunto B no se modifican} en ningún momento durante el proceso que se lleva a cabo con el conjunto A y por tanto la estructura tampoco.

\section{Descripción de los elementos del problema}
\label{ch:01:sec:02}

En el apartado anterior se explico el problema de forma general, sin profundizar en el significado que tiene cada conjunto y los elementos que pertenecen a ellos. A continuación procederemos con una descripción algo más exhaustiva de los conjuntos y sus elementos.

Nos enfrentamos a la resolución de un problema en el campo de la astronomía. Uno de los elementos que se estudian en este campo son las estrellas. El problema abordado intenta corregir los errores observacionales en la fotometría introducidos por los errores del telescopio, condiciones meteorológicas, etcétera. Cada elemento perteneciente a los conjuntos A, B, C y D es una estrella que tiene una serie de datos asociados.

Cada uno de los conjuntos mencionados anteriormente son ficheros de datos, de aquí en adelante nos referiremos a ellos de la siguiente forma. El \textbf{conjunto A} es el \textbf{fichero madre}. El \textbf{conjunto B} es el \textbf{fichero crowding}. Ambos ficheros constituyen la entrada de nuestro problema. El \textbf{conjunto C} es el \textbf{fichero dispersado} y por último el \textbf{conjunto D} es una copia del \textbf{fichero crowding de entrada}, pero, a diferencia de éste, está \textbf{ordenado} (de una determinada forma que explicaremos más adelante) y las estrellas tienen asociado un \textbf{dato adicional}, este fichero será el \textbf{fichero crowding de salida}. Estos dos últimos ficheros constituyen la salida de nuestro problema.

Cada línea del fichero madre representa una \textbf{estrella ‘sintética’, creada con un determinado algoritmo\footnote{El algoritmo está implementado en \cite{IAC-Projects-IACStar}.}, que reproduce un diagrama color-magnitud, con ciertas características a partir de modelos de evolución estelar. Por simplicidad las llamaremos estrellas ‘madre’}. Cada línea del fichero dispersado corresponde a una \textbf{estrella que llamaremos dispersa}. Dado que el fichero crowding de salida es una copia del fichero crowding de entrada e incluye sólo algunos datos adicionales sólo hablaremos de \textbf{estrella crowding}.

Una estrella madre se compone de los siguientes datos \textbf{Magnitud1}, \textbf{Magnitud2}, \textbf{Edad}, \textbf{Metalicidad}, \textbf{X} e \textbf{Y}. Los dos primeros valores son números reales con tres cifras decimales. La Edad es un número natural expresado en notación científica de cuatro cifras significativas. La Metalicidad es un número real expresado en notación científica de cuatro cifras significativas. Los dos últimos valores son números reales con una cifra decimal. Los campos \textbf{Magnitud1} y \textbf{Magnitud2} de esta estrella son magnitudes artificiales; es decir, no tomadas por ningún telescopio y se corresponden con el filtro B y el filtro R, respectivamente del programa \textit{IAC-STAR}.

Una estrella crowding del fichero crowding de entrada se compone de \textbf{Magnitud1}, \textbf{Magnitud2}, \textbf{Magnitud3}, \textbf{Magnitud4}, \textbf{X} e \textbf{Y}. Los cuatro primeros valores son números reales con tres cifras decimales. Los dos últimos valores son números reales con una cifra decimal. Una estrella crowding del fichero crowding de salida tiene los seis datos anteriores y un \textbf{número entero} (cuyo significado será especificado más adelante). Los campos \textbf{Magnitud1} y \textbf{Magnitud2} de esta estrella son las magnitudes artificiales que no tienen errores inyectados en la imagen tomada por el telescopio y se corresponden con el filtro B y el filtro R, respectivamente de la programa \textit{IAC-STAR}.  Los campos \textbf{Magnitud3} y \textbf{Magnitud4} de esta estrella son las magnitudes inyectadas \textbf{Magnitud1} y \textbf{Magnitud2}, respectivamente, recuperadas después de realizar todo el proceso del cálculo de la fotometría de las imágenes. Estas últimas dos magnitudes recuperadas si tienen errores.

Una estrella dispersada se compone de \textbf{Magnitud1’}, \textbf{Magnitud2’}, \textbf{Edad}, \textbf{Metalicidad}, \textbf{X}, \textbf{Y} y \textbf{tres números enteros}. Los dos primeros valores son números reales con tres cifras decimales. La Edad es un número natural expresado en notación científica de cuatro cifras significativas. La Metalicidad es un número real expresado en notación científica de cuatro cifras significativas. Los dos siguientes valores, X e Y, son números reales con una cifra decimal. Explicaremos en detalle cómo se obtienen cada uno de ellos posteriormente. Por último, los tres números enteros indican el resultado del proceso para la estrella madre seleccionada. De nuevo especificaremos cómo se obtienen y su significado posteriormente. El significado de esta estrella es obtener una estrella madre sin que esta esté afectada por errores observacionales en la fotometría.

Los datos que componen cualquiera de las estrellas se disponen en los ficheros de izquierda a derecha en el orden descrito anteriormente. Aunque se ha descrito la representación de los datos a partir del contenido de los ficheros proporcionados por los investigadores del \acrfull{IAC}, se ha brindado soporte para múltiples representaciones numéricas.

Para obtener mayor detalle sobre este proceso y los datos utilizados, el lector puede consultar el documento: \cite{1538-3881-128-3-1465}.

% ---------------------------------------------------------------------
% ---------------------------------------------------------------------
% ---------------------------------------------------------------------