% !TeX root = ../../document.tex
% !TeX encoding = UTF-8
% !TeX spellcheck = es
% ---------------------------------------------------------------------
% ---------------------------------------------------------------------
% ---------------------------------------------------------------------
\chapter{Paralelismo a nivel de \acrshort{CPU} y a nivel de \acrshort{GPU}: OpenMP y CUDA}
\label{ch:06}

En este apartado se describen de forma breve los paradigmas de programación paralela utilizados para acelerar aún más el procesamiento de datos.

\section{OpenMP: Paralelismo a nivel de \acrshort{CPU}}
\label{ch:06:sec:01}

OpenMP (\textbf{Open} \textbf{M}ulti-\textbf{P}rocessing) es una \acrshort{API} para la programación multiproceso de memoria compartida en múltiples plataformas. Permite al programador añadir concurrencia a códigos escritos en \gls{C}, \gls{C++} y \gls{Fortran}. Se basa en el modelo fork-join que proviene de los sistemas Unix, en el que una tarea muy pesada se divide en K hilos de menor peso (fork), al final se recopilan los resultados y se unen en uno solo (join).

OpenMP está disponible para muchas arquitecturas, incluyendo las plataformas Unix/Linux, Windows y MacOSX.

Esta \acrshort{API} se compone de un \textbf{conjunto de directivas de compilación}, \textbf{rutinas de biblioteca} y \textbf{variables de entorno} que influyen en el comportamiento de un programa en tiempo de ejecución.

Ha sido definida de forma conjunta entre distribuidores de hardware y software. Se trata de un modelo de programación portable y escalable, capaz de proporcionar a los desarrolladores una interfaz simple y flexible para que puedan desarrollar aplicaciones paralelas para ordenadores de sobremesa y superordenadores.

La sintaxis básica de una directiva OpenMP para \gls{C}/\gls{C++} es la siguiente:

\begin{code}
  \begin{minted}[bgcolor=bg,xleftmargin=0pt,tabsize=1,breaklines]{c}
# pragma omp nombre-directiva [cláusulas] { bloque de código }
  \end{minted}
\end{code}

Los nombres de directiva más frecuentes son \texttt{parallel}, \texttt{for}, \texttt{sections} y \textit{combinación} de ellos.

Entre las cláusulas más frecuentes están \texttt{private}, \texttt{firstprivate}, \texttt{default}, \texttt{shared}, \texttt{copyin} y \texttt{reduction}. Todas estas cláusulas tienen que ver con la forma en la que se acceden a las variables utilizadas dentro del bloque. Por otra parte la cláusula \texttt{schedule} indica cómo se dividen las iteraciones del for entre los distintos hilos.

Cuando se incluye una directiva OpenMP esto conlleva incluir una sincronización obligatoria en todo el bloque. El bloque de código afectado se marca como paralelo, dentro del bloque trabajaran al mismo tiempo un número de hilos con las características indicadas en la directiva. Al final del bloque habrá una barrera que provocará la sincronización de los todos los hilos (salvo que se indique lo contrario mediante la directiva \texttt{nowait}). Este tipo de ejecución se denomina \textbf{fork-join}.

Para más detalles sobre directivas y cláusulas se puede consultar el link oficial de OpenMP (\url{http://openmp.org/}).

\section{CUDA: Paralelismo a nivel de \acrshort{GPU} Nvidia}
\label{ch:06:sec:02}

CUDA (\textbf{C}ompute \textbf{U}nified \textbf{D}evice \textbf{A}rchitecture) es una plataforma de computación paralela creada por Nvidia. Permite que los desarrolladores de software utilicen las unidades de procesamiento gráfico (\acrshort{GPU}) compatibles con CUDA que se encuentran en las tarjetas gráficas para procesamiento de propósito general. La plataforma CUDA es una capa software que permite un acceso directo al conjunto de instrucciones de la \acrshort{GPU} y a los elementos de computación paralela para la ejecución de \textbf{kernels}.

CUDA es una plataforma que permite a los desarrolladores de software acceder a los dispositivos compatibles con CUDA mediante el uso de lenguajes de programación de alto nivel como \gls{C}, \gls{C++} y \gls{Fortran}. CUDA proporciona un compilador (\textbf{\acrshort{NVCC}}) y una serie de herramientas desarrolladas por Nvidia que permiten a los programadores usar variaciones de los lenguajes anteriores para así poder codificar algoritmos en GPUs Nvidia. El objetivo consiste en facilitar el acceso de los especialistas en programación paralela a los recursos con los que cuentan las GPUs.

También es posible utilizar CUDA en \gls{Python} haciendo uso de PyCUDA, así como en Java usando bindings. CUDA también soporta frameworks de desarrollo como \gls{OpenACC} y \gls{OpenCL}.

La versión inicial de CUDA fue publicada el 23 de Junio de 2007. La última versión estable a día de hoy es la versión 7.5 que fue publicada el 8 de Septiembre de 2015.

La compatibilidad con CUDA se puede encontrar en todas las GPUs Nvidia a partir de  la serie G8X en adelante. Estas GPUs se pueden encontrar en las tarjetas Nvidia GeForce, Tesla y Quadro entre otras.

Según Nvidia, todos aquellos programas que hayan sido desarrollados para la serie GeForce 8 también funcionarán sin \textbf{necesidad de modificación alguna} en todas las tarjetas Nvidia que salgan en un futuro debido a la compatibilidad existente entre tarjetas.

CUDA explota las ventajas de los procesadores gráficos (GPUs) frente a los procesadores de propósito general (\acrshort{CPU}). Las GPUs se componen de una serie de núcleos que permiten un paralelismo masivo siendo capaces de lanzar un altísimo número de hilos simultáneos. Aquellas aplicaciones que hayan sido diseñadas para utilizar un  gran número de hilos de CPU que realizan tareas independientes también podrán usar las GPUs. Las GPUs pueden ser usadas en un gran número de campos tales como la \textbf{medicina, biología, química y un largo etcétera} donde serán capaces de ofrecer un gran rendimiento.

El modelo de programación CUDA permite a las aplicaciones escalar de forma transparente el uso de paralelismo. Este modelo contiene tres puntos claves:

\begin{itemize}
    \item La jerarquía de grupos de hilos.
    \item Las memorias compartidas.
    \item Las barreras de sincronización.
\end{itemize}

La estructura utilizada en este modelo está definida por un grid, en el que hay una serie de bloques de hilos. Los bloques contienen como máximo 1024 hilos distintos.

Cada hilo tiene un identificador único al que se puede acceder con la variable \textbf{threadIdx}. La variable threadIdx tiene tres componentes (x, y, z) que coinciden con las dimensiones de bloques de hilos. Los bloques se identifican a través de la variable \textbf{blockIdx} que tiene dos componentes (x, y).

Los hilos trabajan de forma simultánea dentro de una función especial llamada \textbf{kernel}.

Un ejemplo de definición de un \textit{kernel} en \textbf{CUDA C} es el siguiente:

\begin{code}
  \begin{minted}[bgcolor=bg,xleftmargin=0pt,tabsize=1,breaklines]{c}
__global__ void miKernel(args) { }
  \end{minted}
\end{code}

Para invocar el kernel anterior haríamos algo como:

\begin{code}
  \begin{minted}[bgcolor=bg,xleftmargin=0pt,tabsize=1,breaklines]{c}
miKernel<<gridDim, blockDim>>(args);
  \end{minted}
\end{code}

donde:
\begin{itemize}
    \item \texttt{gridDim} es el número de instancias del kernel.
    \item \texttt{blockDim} es el número de threads de cada instancia.
    \item \texttt{args} es un número limitado de parámetros. Normalmente punteros a vectores en la memoria de la \acrshort{GPU} y constantes copiadas por valor.
\end{itemize}

Todos los hilos trabajan de forma colaborativa unos con otros compartiendo datos dentro de un kernel, por tanto, es necesario disponer de alguna función que permita la sincronización de todos los hilos. Para ello se usa la función \texttt{\_\_syncthreads()} que actúa como una barrera que provoca que todos los hilos esperen a que el resto terminen de ejecutar sus tareas y lleguen al mismo punto. Cuando todos los hilos han llegado a ese punto se levanta la barrera y se sigue con la ejecución de forma normal.

Los hilos pueden acceder a tres memorias diferentes:
\begin{itemize}
    \item Cada hilo puede acceder a una zona de memoria privada.
    \item Cada bloque de hilos tiene una zona de memoria compartida para los hilos que forman el bloque.
    \item Todos los hilos pueden acceder a una memoria global.
\end{itemize}

Hay otros dos espacios de memoria de solo lectura y accesibles por todos los hilos, la \textbf{memoria de constantes y la de texturas}.

La implementación que resuelve el problema mediante paralelismo a nivel de \acrshort{GPU} utilizará CUDA C. Para una mayor documentación de \textbf{CUDA C} se puede consultar el manual de programación en la siguiente URL \url{http://docs.nvidia.com/cuda/cuda-c-programming-guide/}.

% ---------------------------------------------------------------------
% ---------------------------------------------------------------------
% ---------------------------------------------------------------------