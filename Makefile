#----------------------------------------------------------
#
# Makefile.
#
# Para ver las opciones disponibles puedes utilizar:
# $ make help
#
#----------------------------------------------------------

DOCUMENT          = document
SOURCE_DIRECTORY := $(shell pwd)
BUILD_DIRECTORY  := $(shell readlink -f $(SOURCE_DIRECTORY)/build)
#DATE             := $(shell date +"%Y%m%d%H%M%S")
DATE             := ""

#----------------------------------------------------------
#
# Genera el documento utilizando pdflatex
# 
#----------------------------------------------------------
pdflatex: clean
	@echo
	@echo "Compilacion iniciada ..."
	@mkdir -p $(BUILD_DIRECTORY)
	@ln -fs $(BUILD_DIRECTORY)/${DOCUMENT}.pyg ${DOCUMENT}.pyg
	pdflatex \
           -shell-escape \
           -output-directory=$(BUILD_DIRECTORY) \
           $(DOCUMENT)
	-biber \
           --input-directory=$(BUILD_DIRECTORY) \
           --output-directory=$(BUILD_DIRECTORY) \
           $(DOCUMENT)
	-makeglossaries \
           -d $(BUILD_DIRECTORY) \
           $(DOCUMENT)
	-@cd $(BUILD_DIRECTORY)  && \
	makeindex $(DOCUMENT)    && \
	cd $(SOURCE_DIRECTORY)
	@ln -fs $(BUILD_DIRECTORY)/${DOCUMENT}.pyg ${DOCUMENT}.pyg
	pdflatex \
           -shell-escape \
           -output-directory=$(BUILD_DIRECTORY) \
           $(DOCUMENT)
	@ln -fs $(BUILD_DIRECTORY)/${DOCUMENT}.pyg ${DOCUMENT}.pyg
	pdflatex \
           -shell-escape \
           -output-directory=$(BUILD_DIRECTORY) \
           $(DOCUMENT)
	@rm -fr ${DOCUMENT}.pyg $(SOURCE_DIRECTORY)/_minted-$(DOCUMENT)
	@echo "Compilacion finalizada"
	@mv $(BUILD_DIRECTORY)/$(DOCUMENT).pdf $(SOURCE_DIRECTORY)/$(DOCUMENT)_$(DATE).pdf
	@echo


#----------------------------------------------------------
#
# Muestra los directorios implicados
#
#----------------------------------------------------------
echo:
	@echo
	@echo "Los direcctorios implicados son ..."
	@echo "   SOURCE_DIRECTORY : $(SOURCE_DIRECTORY)"
	@echo "   BUILD_DIRECTORY  : $(BUILD_DIRECTORY)"
	@echo

#----------------------------------------------------------
#
# Elimina el directorio de salida
#
#----------------------------------------------------------
clean-pdf:
	@echo
	@echo "Elimina los ficheros *.pdf ..."
	@rm -fr $(SOURCE_DIRECTORY)/*.pdf
	@echo

#----------------------------------------------------------
#
# Elimina el directorio de salida
#
#----------------------------------------------------------
clean:
	@echo
	@echo "Elimina el directorio de salida ..."
	@rm -fr $(BUILD_DIRECTORY)
	@echo

#----------------------------------------------------------
#
# Muestra una ayuda de las opciones disponibles
#
#----------------------------------------------------------
help:
	@echo 
	@echo Objetivos disponibles:
	@echo 
	@echo "   pdflatex [predefinido] : Genera el pdf usando pdflatex"
	@echo "   echo : Elimina el directorio de salida"
	@echo "   clean : Elimina el directorio de salida"
	@echo "   help : Muestra esta ayuda"
	@echo
